This directory contains GitLab-ci templates, scripts used to generate jobs and linter config files.

`scripts`: scripts used in CI
`configuration`: configuration files used in CI
